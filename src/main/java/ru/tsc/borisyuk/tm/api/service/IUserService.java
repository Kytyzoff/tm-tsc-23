package ru.tsc.borisyuk.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.borisyuk.tm.enumerated.Role;
import ru.tsc.borisyuk.tm.model.User;

public interface IUserService extends IService<User> {

    @Nullable
    User findByEmail(@Nullable String email);

    boolean isLoginExists(@Nullable String login);

    boolean isEmailExists(@Nullable String email);

    @Nullable
    User findByLogin(@Nullable String login);

    @Nullable
    User removeByLogin(@Nullable String login);

    @NotNull
    User create(@Nullable String login, @Nullable String password);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @Nullable
    User setPassword(@Nullable String userId, @Nullable String password);

    @Nullable
    User updateUser(@Nullable String userId, @Nullable String firstName, @Nullable String lastName, @Nullable String middleName);

    @Nullable
    User lockUserByLogin(@Nullable String login);

    @Nullable
    User unlockUserByLogin(@Nullable String login);

}
