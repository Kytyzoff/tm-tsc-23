package ru.tsc.borisyuk.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.borisyuk.tm.model.AbstractOwnerEntity;

import java.util.Comparator;
import java.util.List;

public interface IOwnerService<E extends AbstractOwnerEntity> extends IService<E> {

    void add(@Nullable String userId, @Nullable E entity);

    void remove(@Nullable String userId, @Nullable E entity);

    @NotNull
    List<E> findAll(@Nullable String userId);

    @NotNull
    List<E> findAll(@Nullable String userId, @Nullable Comparator<E> comparator);

    void clear(@Nullable String userId);

    @Nullable
    E findById(@Nullable String userId, @Nullable String id);

    @NotNull
    E findByIndex(@Nullable String userId, @Nullable Integer index);

    @Nullable
    E removeById(@Nullable String userId, @Nullable String id);

    @Nullable
    E removeByIndex(@Nullable String userId, @Nullable Integer index);

    boolean existsById(@Nullable String userId, @Nullable String id);

    boolean existsByIndex(@Nullable String userId, @NotNull Integer index);

}
