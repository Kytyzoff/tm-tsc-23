package ru.tsc.borisyuk.tm.comparator;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.borisyuk.tm.api.entity.IHasStartDate;

import java.util.Comparator;

@NoArgsConstructor
public final class ComparatorByStartDate implements Comparator<IHasStartDate> {

    @NotNull
    private static final ComparatorByStartDate INSTANCE = new ComparatorByStartDate();

    @NotNull
    public static ComparatorByStartDate getInstance() {
        return INSTANCE;
    }

    @Override
    public int compare(@Nullable final IHasStartDate o1, @Nullable final IHasStartDate o2) {
        if (o1 == null || o2 == null) return 0;
        return o1.getStartDate().compareTo(o2.getStartDate());
    }

}
