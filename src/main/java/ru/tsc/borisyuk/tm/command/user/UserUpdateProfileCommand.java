package ru.tsc.borisyuk.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.borisyuk.tm.command.AbstractCommand;
import ru.tsc.borisyuk.tm.enumerated.Role;
import ru.tsc.borisyuk.tm.model.User;
import ru.tsc.borisyuk.tm.util.TerminalUtil;

public class UserUpdateProfileCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "update-user";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "update user's profile...";
    }

    @Override
    public void execute() {
        final User user = serviceLocator.getAuthService().getUser();
        System.out.println("[UPDATE PROFILE]");
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("ENTER FIRST NAME: ");
        final String firstName = TerminalUtil.nextLine();
        System.out.println("ENTER LAST NAME: ");
        final String lastName = TerminalUtil.nextLine();
        System.out.println("ENTER MIDDLE NAME: ");
        final String middleName = TerminalUtil.nextLine();
        serviceLocator.getUserService().updateUser(userId, firstName, lastName, middleName);
        System.out.println("[OK]");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
