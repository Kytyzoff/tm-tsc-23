package ru.tsc.borisyuk.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.borisyuk.tm.api.service.IAuthService;
import ru.tsc.borisyuk.tm.api.service.IUserService;
import ru.tsc.borisyuk.tm.enumerated.Role;
import ru.tsc.borisyuk.tm.exception.empty.EmptyLoginException;
import ru.tsc.borisyuk.tm.exception.empty.EmptyPasswordException;
import ru.tsc.borisyuk.tm.exception.user.AccessDeniedException;
import ru.tsc.borisyuk.tm.model.User;
import ru.tsc.borisyuk.tm.util.HashUtil;

public class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private String userId;

    public AuthService(final IUserService userService) {
        this.userService = userService;
    }

    @Nullable
    @Override
    public User getUser() {
        final String userId = getUserId();
        return userService.findById(userId);
    }

    @NotNull
    @Override
    public String getUserId() {
        if (userId == null) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public boolean isAuth() {
        return userId == null;
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public void login(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final User user = userService.findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        final String hash = HashUtil.salt(password);
        if (hash == null) throw new AccessDeniedException();
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        userId = user.getId();
    }

    @Override
    public void checkRoles(@Nullable final Role... roles) {
        if (roles == null || roles.length == 0) return;
        final User user = getUser();
        if (user == null) throw new AccessDeniedException();
        final Role role = user.getRole();
        if (role == null) throw new AccessDeniedException();
        for (final Role item : roles) {
            if (item.equals(role)) return;
        }
        throw new AccessDeniedException();
    }

    @Override
    public void registry(String login, String password, String email) {
        userService.create(login, password, email);
    }

}
