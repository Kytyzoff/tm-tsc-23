package ru.tsc.borisyuk.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.borisyuk.tm.api.repository.IOwnerRepository;
import ru.tsc.borisyuk.tm.api.service.IOwnerService;
import ru.tsc.borisyuk.tm.exception.empty.EmptyIdException;
import ru.tsc.borisyuk.tm.exception.empty.EmptyIndexException;
import ru.tsc.borisyuk.tm.exception.entity.EntityNotFoundException;
import ru.tsc.borisyuk.tm.exception.user.EmptyUserIdException;
import ru.tsc.borisyuk.tm.model.AbstractOwnerEntity;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractOwnerService<E extends AbstractOwnerEntity> extends AbstractService<E> implements IOwnerService<E> {

    @NotNull
    private final IOwnerRepository<E> repository;

    public AbstractOwnerService(@NotNull final IOwnerRepository<E> repository) {
        super(repository);
        this.repository = repository;
    }

    @Override
    public void add(@Nullable final String userId, @Nullable final E entity) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (entity == null) throw new EntityNotFoundException();
        repository.add(userId, entity);
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final E entity) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (entity == null) throw new EntityNotFoundException();
        repository.remove(userId, entity);
    }

    @Override
    public List<E> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return repository.findAll(userId);
    }

    @Override
    public List<E> findAll(@Nullable final String userId, @Nullable final Comparator<E> comparator) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (comparator == null) return Collections.emptyList();
        return repository.findAll(userId, comparator);
    }

    @Override
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        repository.clear(userId);
    }

    @Nullable
    @Override
    public E findById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.findById(userId, id);
    }

    @NotNull
    @Override
    public E findByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        return repository.findByIndex(userId, index);
    }

    @Nullable
    @Override
    public E removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.removeById(userId, id);
    }

    @Nullable
    @Override
    public E removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        return repository.removeByIndex(userId, index);
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) return false;
        return repository.existsById(userId, id);
    }

    @Override
    public boolean existsByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return repository.existsByIndex(userId, index);
    }

}
